package com.meal.app;

import com.meal.service.ConsoleService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author aladin
 * @since 2/10/20
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ConsoleService consoleService = new ConsoleService();
        consoleService.loadDriver();
        try {
            consoleService.consoleInput(connection, statement);
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
    }
}