package com.meal.domain;

/**
 * @author aladin
 * @since 2/12/20
 */
public class MealTime {

    private int id;
    private String mealTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }
}
