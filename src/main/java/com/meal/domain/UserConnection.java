package com.meal.domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author aladin
 * @since 2/10/20
 */
public class UserConnection {

    private String url;
    private String user;
    private String password;
    private Connection connection;

    public UserConnection() throws SQLException {
        url = "jdbc:mysql://localhost:3306/meal__system?serverTimezone=UTC";
        user = "aladinSojon";
        password = "AladinSojon59:)";
        connection = DriverManager.getConnection(url, user, password);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
