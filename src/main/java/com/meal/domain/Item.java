package com.meal.domain;

/**
 * @author aladin
 * @since 2/12/20
 */
public class Item {

    private int id;
    private String item;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }


}
