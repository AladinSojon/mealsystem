package com.meal.domain;

import java.util.ArrayList;
import java.util.List;

public class MealMenu {

    private MealTime mealTime;
    private Day day;
    private List<Item> itemList = new ArrayList<>();

    public MealMenu(MealTime mealTime, Day day, List<Item> itemList) {
        this.mealTime = mealTime;
        this.day = day;
        this.itemList = itemList;
    }

    public MealTime getMealTime() {
        return mealTime;
    }

    public Day getDay() {
        return day;
    }

    public List<Item> getItemList() {
        return itemList;
    }
}
