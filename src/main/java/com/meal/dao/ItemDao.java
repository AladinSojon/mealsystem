package com.meal.dao;

import com.meal.domain.Item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author aladin
 * @since 2/13/20
 */
public class ItemDao {

    public static ResultSet forShow(Connection connection, PreparedStatement statement) throws SQLException {
        String menuQuery = "SELECT * FROM items";
        statement = connection.prepareStatement(menuQuery);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public static void addItem(Connection connection, PreparedStatement statement, Item item) throws SQLException {
        String insertQuery = "insert into items values('" + item.getId() + "','" + item.getItem() + "')";
        statement = connection.prepareStatement(insertQuery);
        statement.executeUpdate();
    }

    public static void forUpdateItem(Connection connection, PreparedStatement updateStatement, Item item, Item updatedItem) throws SQLException {
        String updateQuery = "UPDATE items SET item_name=? WHERE item_name=?";
        updateStatement = connection.prepareStatement(updateQuery);
        updateStatement.setString(1, updatedItem.getItem());
        updateStatement.setString(2, item.getItem());
        updateStatement.addBatch();
        updateStatement.executeBatch();
    }

    public static void forDeleteItem(Connection connection, PreparedStatement statement, Item item) throws SQLException {
        String deleteQuery = "Delete FROM meal_menu WHERE id_item=?";
        deleteHelper(connection, statement, item, deleteQuery);
        String deleteQuery2 = "Delete FROM items WHERE id_item=?";
        deleteHelper(connection, statement, item, deleteQuery2);
    }

    public static void deleteHelper(Connection connection, PreparedStatement statement, Item item, String deleteQuery) throws SQLException {
        statement = connection.prepareStatement(deleteQuery);
        statement.setInt(1, item.getId());
        statement.addBatch();
        statement.executeBatch();
    }
}
