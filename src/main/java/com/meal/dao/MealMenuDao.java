package com.meal.dao;

import com.meal.domain.MealMenu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author aladin
 * @since 2/13/20
 */
public class MealMenuDao {

    public static void updateMeal(Connection connection, PreparedStatement statement, MealMenu mealMenu) throws SQLException {
        String updateQuery = "UPDATE meal_menu SET id_item=? WHERE id_day=? and id_meal_time=? and id_item=?";
        statement = connection.prepareStatement(updateQuery);
        statement.setInt(1, mealMenu.getItemList().get(1).getId());
        statement.setInt(2, mealMenu.getDay().getId());
        statement.setInt(3, mealMenu.getMealTime().getId());
        statement.setInt(4, mealMenu.getItemList().get(0).getId());
        statement.addBatch();
        statement.executeBatch();
    }

    public static ResultSet showMenu(Connection connection, PreparedStatement statement) throws SQLException {
        String menuQuery = "SELECT days.id_day, days.name_day, meal_time.id_meal, meal_time.meal_time_name, items.id_item,items.item_name FROM meal_menu,days,meal_time,items " +
                "WHERE meal_menu.id_day = days.id_day AND meal_menu.id_meal_time = meal_time.id_meal AND meal_menu.id_item = items.id_item";
        statement = connection.prepareStatement(menuQuery);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public static void insertMeal(Connection connection, PreparedStatement statement, MealMenu mealMenu) throws SQLException {
        String insertQuery = "INSERT INTO meal_menu VALUES('" + mealMenu.getDay().getId() + "','" + mealMenu.getMealTime().getId() + "','" + mealMenu.getItemList().get(0).getId() + "')";
        statement = connection.prepareStatement(insertQuery);
        statement.executeUpdate();
    }

    public static void deleteMeal(Connection connection, PreparedStatement deleteStatement, MealMenu mealMenu) throws SQLException {
        String deleteQuery = "DELETE FROM meal_menu WHERE id_day=? AND id_meal_time=? AND id_item=?";
        deleteStatement = connection.prepareStatement(deleteQuery);
        deleteStatement.setInt(1, mealMenu.getDay().getId());
        deleteStatement.setInt(2, mealMenu.getMealTime().getId());
        deleteStatement.setInt(3, mealMenu.getItemList().get(0).getId());
        deleteStatement.addBatch();
        deleteStatement.executeBatch();
    }
}
