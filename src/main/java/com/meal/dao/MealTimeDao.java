package com.meal.dao;

import com.meal.domain.MealTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author aladin
 * @since 2/13/20
 */
public class MealTimeDao {

    public static ResultSet forShow(Connection connection, PreparedStatement statement) throws SQLException {
        String menuQuery = "select * from meal_time";
        statement = connection.prepareStatement(menuQuery);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public static void forCreate(Connection connection, PreparedStatement statement, MealTime mealTime) throws SQLException {
        String insertQuery = "INSERT INTO meal_time VALUES('" + mealTime.getId() + "','" + mealTime.getMealTime() + "')";
        System.out.println(insertQuery);
        statement = connection.prepareStatement(insertQuery);
        statement.executeUpdate();
    }

    public static void forUpdate(Connection connection, PreparedStatement statement, MealTime mealTime, MealTime updatedMealTime) throws SQLException {
        String updateQuery = "UPDATE meal_time SET meal_time_name=? WHERE meal_time_name=?";
        statement = connection.prepareStatement(updateQuery);
        statement.setString(1, updatedMealTime.getMealTime());
        statement.setString(2, mealTime.getMealTime());
        System.out.println(updateQuery + "  " + updatedMealTime.getMealTime() + "   " + mealTime.getMealTime());
        statement.addBatch();
        statement.executeBatch();
    }

    public static void forDelete(Connection connection, PreparedStatement deleteStatement, MealTime mealTime) throws SQLException {
        String deleteQuery = "Delete FROM meal_menu WHERE id_meal_time=?";
        System.out.println(deleteQuery);
        deleteHelper(connection, deleteStatement, mealTime, deleteQuery);

        String deleteQuery2 = "Delete from meal_time WHERE id_meal=?";
        deleteHelper(connection, deleteStatement, mealTime, deleteQuery2);
    }

    private static void deleteHelper(Connection connection, PreparedStatement deleteStatement, MealTime mealTime, String deleteQuery) throws SQLException {
        deleteStatement = connection.prepareStatement(deleteQuery);
        deleteStatement.setInt(1, mealTime.getId());
        deleteStatement.addBatch();
        deleteStatement.executeBatch();
    }
}
