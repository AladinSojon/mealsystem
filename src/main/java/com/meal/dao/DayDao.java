package com.meal.dao;

import com.meal.domain.Day;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author aladin
 * @since 2/13/20
 */
public class DayDao {
    public static ResultSet forShow(Connection connection, PreparedStatement statement) throws SQLException {
        String menuQuery = "SELECT * FROM days";
        statement = connection.prepareStatement(menuQuery);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public static void addDay(Connection connection, PreparedStatement statement, Day day) throws SQLException {
        String insertQuery = "insert into days values('" + day.getId() + "','" + day.getName() + "')";
        statement = connection.prepareStatement(insertQuery);
        statement.executeUpdate();
    }

    public static void forUpdate(Connection connection, PreparedStatement updateStatement, Day day, Day updatedDay) throws SQLException {
        String updateQuery = "UPDATE days SET name_day=? WHERE name_day=?";
        updateStatement = connection.prepareStatement(updateQuery);
        updateStatement.setString(1, updatedDay.getName());
        updateStatement.setString(2, day.getName());
        updateStatement.addBatch();
        updateStatement.executeBatch();
    }

    public static void forDeleteDay(Connection connection, PreparedStatement statement, Day day) throws SQLException {
        String deleteQuery = "Delete FROM meal_menu WHERE id_day=?";
        deleteHelper(connection, statement, day, deleteQuery);
        String deleteQuery2 = "Delete FROM days WHERE id_day=?";
        deleteHelper(connection, statement, day, deleteQuery2);
    }

    public static void deleteHelper(Connection connection, PreparedStatement statement, Day day, String deleteQuery) throws SQLException {
        statement = connection.prepareStatement(deleteQuery);
        statement.setInt(1, day.getId());
        statement.addBatch();
        statement.executeBatch();
    }
}
