package com.meal.service;

import com.meal.dao.MealMenuDao;
import com.meal.domain.Day;
import com.meal.domain.Item;
import com.meal.domain.MealMenu;
import com.meal.domain.MealTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author aladin
 * @since 2/13/20
 */
public class MealMenuService {

    public void printMenu(Connection connection, PreparedStatement statement) throws SQLException {
        ResultSet resultSet = MealMenuDao.showMenu(connection, statement);
        printMenuIO(resultSet);
    }

    public void printMenuIO(ResultSet resultSet) throws SQLException {
        System.out.println("---------------------------------------------------------");
        System.out.println("|                     Meal Menu                         |");
        System.out.println("---------------------------------------------------------");

        System.out.println("| id     Day       | id   Meal Time   | id  Item Name   |");
        System.out.println("---------------------------------------------------------");

        while (resultSet.next()) {
            System.out.println(String.format("| %s  %13s | %s  %13s | %s %14s|", resultSet.getInt("id_day"), resultSet.getString("name_day"),
                    resultSet.getInt("id_meal"), resultSet.getString("meal_time_name"), resultSet.getInt("id_item"), resultSet.getString("item_name")));
        }
        System.out.println("---------------------------------------------------------");
    }

    public void mealUpdateIO(Connection connection, PreparedStatement statement, Scanner scanner) throws SQLException {
        Day day = new Day();
        List<Item> items = new ArrayList<>();
        MealTime mealTime = new MealTime();
        MealMenu mealMenu = new MealMenu(mealTime, day, items);

        System.out.print("Meal Time : ");
        mealMenu.getMealTime().setId(scanner.nextInt());
        System.out.print("Day: ");
        mealMenu.getDay().setId(scanner.nextInt());

        System.out.print("Item: ");
        Item item = new Item();
        item.setId(scanner.nextInt());
        mealMenu.getItemList().add(item);

        System.out.print("Item to update: ");
        Item updatedItem = new Item();
        updatedItem.setId(scanner.nextInt());
        mealMenu.getItemList().add(updatedItem);
        mealUpdate(connection, statement, mealMenu);
    }

    public void mealUpdate(Connection connection, PreparedStatement statement, MealMenu mealMenu) throws SQLException {
        MealMenuDao.updateMeal(connection, statement, mealMenu);
    }

    public void mealInsertIO(Connection connection, PreparedStatement insertStatement) throws SQLException {
        Day days = new Day();
        List<Item> items = new ArrayList<>();
        MealTime mealTime = new MealTime();
        Scanner scanner = new Scanner(System.in);
        MealMenu mealMenu = new MealMenu(mealTime, days, items);

        System.out.print("Meal Time: ");
        mealMenu.getMealTime().setId(scanner.nextInt());
        System.out.print("Day: ");
        mealMenu.getDay().setId(scanner.nextInt());

        System.out.print("Item to add: ");
        Item item = new Item();
        item.setId(scanner.nextInt());
        mealMenu.getItemList().add(item);
        mealInsert(connection, insertStatement, mealMenu);
    }

    public void mealInsert(Connection connection, PreparedStatement statement, MealMenu mealMenu) throws SQLException {
        MealMenuDao.insertMeal(connection, statement, mealMenu);
    }

    public void mealDeleteIO(Connection connection, PreparedStatement deleteStatement, Scanner scanner) throws SQLException {
        MealTime mealTime = new MealTime();
        Day day = new Day();
        List<Item> items = new ArrayList<>();
        MealMenu mealMenu = new MealMenu(mealTime, day, items);

        System.out.print("Meal Time: ");
        mealMenu.getMealTime().setId(scanner.nextInt());

        System.out.print("Day: ");
        mealMenu.getDay().setId(scanner.nextInt());

        System.out.print("Item to delete: ");
        Item item = new Item();
        item.setId(scanner.nextInt());
        mealMenu.getItemList().add(item);
        mealDelete(connection, deleteStatement, mealMenu);
    }

    public void mealDelete(Connection connection, PreparedStatement deleteStatement, MealMenu mealMenu) throws SQLException {
        MealMenuDao.deleteMeal(connection, deleteStatement, mealMenu);
    }
}