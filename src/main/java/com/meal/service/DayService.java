package com.meal.service;

import com.meal.dao.DayDao;
import com.meal.domain.Day;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class DayService {

    public Day day;

    public DayService() {
        day = new Day();
    }

    public void showDay(Connection connection, PreparedStatement statement) throws SQLException {
        ResultSet resultSet = DayDao.forShow(connection, statement);
        showDayIO(resultSet);
    }

    public void showDayIO(ResultSet resultSet) throws SQLException {
        System.out.println("---------------------");
        System.out.println("| id |    Day     |");
        System.out.println("---------------------");
        while (resultSet.next()) {
            System.out.println(String.format("|%3s | %12s |", resultSet.getInt("id_day"), resultSet.getString("name_day")));
        }
        System.out.println("---------------------");
    }

    public void addDayIO(Connection connection, PreparedStatement statement, Scanner scanner) throws SQLException {
        System.out.print("Create id: ");
        day.setId(scanner.nextInt());
        scanner.nextLine();
        System.out.print("Create Day: ");
        day.setName(scanner.nextLine());
        addDay(connection, statement, day);
    }

    public void addDay(Connection connection, PreparedStatement statement, Day day) throws SQLException {
        DayDao.addDay(connection, statement, day);
    }

    public void updateDayIO(Connection connection, PreparedStatement updateStatement, Scanner scanner) throws SQLException {
        scanner.nextLine();
        System.out.print("Day: ");
        day.setName(scanner.nextLine());
        Day updatedDay = new Day();
        System.out.print("Day to update: ");
        updatedDay.setName(scanner.nextLine());
        updateDay(connection, updateStatement, day, updatedDay);
    }

    public void updateDay(Connection connection, PreparedStatement updateStatement, Day day, Day updatedDay) throws SQLException {
        DayDao.forUpdate(connection, updateStatement, day, updatedDay);
    }

    public void dayDeleteIO(Connection connection, PreparedStatement statement, Scanner scanner) throws SQLException {
        System.out.print("Day: ");
        day.setId(scanner.nextInt());
        dayDelete(connection, statement, day);
    }

    public void dayDelete(Connection connection, PreparedStatement statement, Day day) throws SQLException {
        DayDao.forDeleteDay(connection, statement, day);
    }
}