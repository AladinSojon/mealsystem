package com.meal.service;

import com.meal.dao.ItemDao;
import com.meal.domain.Item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class ItemService {

    public Item item;

    public ItemService() {
        item = new Item();
    }

    public void showItemList(Connection connection, PreparedStatement statement) throws SQLException {
        ResultSet resultSet = ItemDao.forShow(connection, statement);
        showItemListIO(resultSet);
    }

    public void showItemListIO(ResultSet resultSet) throws SQLException {
        System.out.println("---------------------");
        System.out.println("| id |    Items     |");
        System.out.println("---------------------");

        while (resultSet.next()) {
            System.out.println(String.format("|%3s | %12s |", resultSet.getInt("id_item"), resultSet.getString("item_name")));
        }
        System.out.println("---------------------");
    }

    public void addItemToItemsIO(Connection connection, PreparedStatement statement, Scanner scanner) throws SQLException {
        System.out.print("Create id id: ");
        item.setId(scanner.nextInt());
        scanner.nextLine();
        System.out.print("Create item: ");
        item.setItem(scanner.nextLine());
        addItemToItems(connection, statement, item);
    }

    public void addItemToItems(Connection connection, PreparedStatement statement, Item item) throws SQLException {
        ItemDao.addItem(connection, statement, item);
    }

    public void updateItemListIO(Connection connection, PreparedStatement updateStatement, Scanner scanner) throws SQLException {
        scanner.nextLine();
        System.out.print("Item: ");
        item.setItem(scanner.nextLine());
        Item updatedItem = new Item();
        System.out.print("Item to update: ");
        updatedItem.setItem(scanner.nextLine());
        updateItemList(connection, updateStatement, item, updatedItem);
    }

    public void updateItemList(Connection connection, PreparedStatement updateStatement, Item item, Item updatedItem) throws SQLException {
        ItemDao.forUpdateItem(connection, updateStatement, item, updatedItem);
    }

    public void itemDeleteIO(Connection connection, PreparedStatement statement, Scanner scanner) throws SQLException {
        System.out.print("Item: ");
        item.setId(scanner.nextInt());
        itemDelete(connection, statement, item);
    }

    public void itemDelete(Connection connection, PreparedStatement statement, Item item) throws SQLException {
        ItemDao.forDeleteItem(connection, statement, item);
    }
}
