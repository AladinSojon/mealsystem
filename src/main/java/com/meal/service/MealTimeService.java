package com.meal.service;

import com.meal.dao.MealTimeDao;
import com.meal.domain.MealTime;
import com.sun.corba.se.pept.transport.ConnectionCache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class MealTimeService {

    public MealTime mealTime;

    public MealTimeService() {
        mealTime = new MealTime();
    }

    public void showMealTime(Connection connection, PreparedStatement statement) throws SQLException {
        ResultSet resultSet = MealTimeDao.forShow(connection, statement);
        showMealTimeIO(resultSet);
    }

    public void showMealTimeIO(ResultSet resultSet) throws SQLException {
        System.out.println("--------------------");
        System.out.println("| id |  Meal Time   |");
        System.out.println("---------------------");

        while (resultSet.next()) {
            System.out.println(String.format("|%3s |%13s |", resultSet.getInt("id_meal"), resultSet.getString("meal_time_name")));
        }
        System.out.println("--------------------");
    }

    public void mealCreateIO(Connection connection, PreparedStatement createStatement, Scanner scanner) throws SQLException {
        System.out.print("Create Meal Time id: ");
        mealTime.setId(scanner.nextInt());
        scanner.nextLine();
        System.out.print("Create Meal Time: ");
        mealTime.setMealTime(scanner.nextLine());
        mealCreate(connection, createStatement, mealTime);
    }

    public void mealCreate(Connection connection, PreparedStatement statement, MealTime mealTime) throws SQLException {
        MealTimeDao.forCreate(connection, statement, mealTime);
    }

    public void mealTimeUpdateIO(Connection connection, PreparedStatement statement, Scanner scanner) throws SQLException {
        scanner.nextLine();
        System.out.print("Meal Time: ");
        mealTime.setMealTime(scanner.nextLine());
        System.out.print("Meal Time to update: ");
        MealTime updatedMealTime = new MealTime();
        updatedMealTime.setMealTime(scanner.nextLine());
        mealTimeUpdate(connection, statement, mealTime, updatedMealTime);
    }

    public void mealTimeUpdate(Connection connection, PreparedStatement statement, MealTime mealTime, MealTime updatedMealTime) throws SQLException {
        MealTimeDao.forUpdate(connection, statement, mealTime, updatedMealTime);

    }

    public void deleteMealTimeIO(Connection connection, PreparedStatement deleteStatement, Scanner scanner) throws SQLException {
        System.out.println("Delete Meal Time id: ");
        mealTime.setId(scanner.nextInt());
        deleteMealTime(connection, deleteStatement, mealTime);
    }

    public void deleteMealTime(Connection connection, PreparedStatement deleteStatement, MealTime mealTime) throws SQLException {
        MealTimeDao.forDelete(connection, deleteStatement, mealTime);

    }
}