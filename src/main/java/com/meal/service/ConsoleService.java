package com.meal.service;

import com.meal.domain.UserConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author aladin
 * @since 2/13/20
 */
public class ConsoleService {

    public void consoleInput(Connection connection, PreparedStatement statement) throws SQLException {
        MealMenuService mealMenuService = new MealMenuService();
        ItemService itemService = new ItemService();
        DayService dayService = new DayService();
        MealTimeService mealTimeService = new MealTimeService();

        UserConnection menu = new UserConnection();
        connection = menu.getConnection();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Press 1 to see the meal menu");
        System.out.println("Press 2 to add meal to menu");
        System.out.println("Press 3 to update menu");
        System.out.println("Press 4 to delete meal from menu");

        System.out.println("Press 5 to see meal times");
        System.out.println("Press 6 to create new meal time");
        System.out.println("Press 7 to update meal time");
        System.out.println("Press 8 to delete meal time");

        System.out.println("Press 9 to see item list");
        System.out.println("Press 10 to add item to item list");
        System.out.println("Press 11 to update item name");
        System.out.println("Press 12 to delete item from item list");

        System.out.println("Press 13 to see days");
        System.out.println("Press 14 to add day to day list");
        System.out.println("Press 15 to update day");
        System.out.println("Press 16 to delete day");

        int input;
        while (true) {
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    mealMenuService.printMenu(connection, statement);
                    break;
                case 2:
                    mealMenuService.mealInsertIO(connection, statement);
                    break;
                case 3:
                    mealMenuService.mealUpdateIO(connection, statement, scanner);
                    break;
                case 4:
                    mealMenuService.mealDeleteIO(connection, statement, scanner);
                    break;
                case 5:
                    mealTimeService.showMealTime(connection, statement);
                    break;
                case 6:
                    mealTimeService.mealCreateIO(connection, statement, scanner);
                    break;
                case 7:
                    mealTimeService.mealTimeUpdateIO(connection, statement, scanner);
                    break;
                case 8:
                    mealTimeService.deleteMealTimeIO(connection, statement, scanner);
                    break;
                case 9:
                    itemService.showItemList(connection, statement);
                    break;
                case 10:
                    itemService.addItemToItemsIO(connection, statement, scanner);
                    break;
                case 11:
                    itemService.updateItemListIO(connection, statement, scanner);
                    break;
                case 12:
                    itemService.itemDeleteIO(connection, statement, scanner);
                    break;
                case 13:
                    dayService.showDay(connection,statement);
                    break;
                case 14:
                    dayService.addDayIO(connection,statement,scanner);
                    break;
                case 15:
                    dayService.updateDayIO(connection, statement, scanner);
                    break;
                case 16:
                    dayService.dayDeleteIO(connection, statement, scanner);
            }
        }
    }

    public void loadDriver() throws ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
    }
}
